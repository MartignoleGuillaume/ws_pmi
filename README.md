Requirements
------------

[ROS Kinetic Installation](http://wiki.ros.org/kinetic/Installation/Ubuntu)


Installation
------------

* Step 1 : Install ROS catkin System build ::
```
sudo apt-get install ros-kinetic-catkin python-catkin-tools
```


* Step 2 : Install MoveIt ::
```  
  sudo apt install ros-kinetic-moveit
```

* Step 3 : Clone the repository ::
```
git clone
```

* Step 4 : Go in the PMI Workspace ::
```
  cd ~/ws_pmi
```
* Step 5 : Configure and Build the PMI Workspace ::
```
  catkin config --extend /opt/ros/kinetic
  catkin build
```
* Step 6 : Source and Build the PMI Workspace ::
``` 
  source ~/ws_pmi/devel/setup.bash
```
* Step 7 : Run the Simulation ::
``` 
  roslaunch pmi_config demo.launch
```
  
  
