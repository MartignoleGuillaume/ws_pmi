%% Load and display robot
% plot(time_sine_wave,sine_wave,'bo-')
% plot(time_sine_wave,x,time_sine_wave,y,time_sine_wave,z,'bo-')
% legend('x','y','z')
clear 
clc 

%% Rigid Body  


open_system('robotarm.slx');
[robot,importInfo]=importrobot(gcs)
showdetails(robot)
axes = show(robot)

%% Create a set of desired wayPoints
wayPoints = [0 0 0.3;0 0 0.3;0 0 0.4022;0 0 0.4496;0 0 0.4167;0 0 0.3212;0 0 0.2142;
    0 0 0.1533;0 0 0.1712;0 0 0.2581;0 0 0.3675;0 0 0.4407;0 0 0.4384;0 0 0.3618;
    0 0 0.2521;0 0 0.1680;0 0 0.1548;0 0 0.2195;0 0 0.3274;0 0 0.4206;0 0 0.4491];
hold on 
trajectory = cscvn(wayPoints'); % Create a smooth curve of the trajectory of the robot 
fnplt(trajectory, 'r');

% Evaluate trajectory to create a vector of base
numTotalPoints = 30; 
eePositions = ppval(trajectory,linspace(0,trajectory.breaks(20),numTotalPoints)); 
%% Inverse kinematics

ik =  robotics.InverseKinematics('RigidBodyTree', robot); 
weights = [0.5 0.5 0.5 1 1 1];
initialguess = robot.homeConfiguration;

for idx = 1:size(eePositions,2)
    
    tform = trvec2tform(eePositions(:,idx)');
    configSoln(idx,:) = ik('Base',tform,weights,initialguess); 
    initialguess = configSoln(idx,:);      
end

%% Visualize robot configurations
title('Robot waypoint tracking visualization')
for idx = 1:size(trajectory,2)
    show(robot,configSoln(idx,:));
    pause(0.1)
end
hold off









%%
% %% (robot, homeConfiguration(robot), 'Base','Body4');
%load_system ("Robot_Arm_endfactor_z_trans.slx")
% axes=show(robot);
% axes.CameraPositionMode = 'auto';
% time=0:0.5:10; 
% x_socle = [time' zeros(21,1)]; 
% y_socle = [time' zeros(21,1)]; 
% z_socle = [time' [0.3000;0.3000;0.4022;0.4496;0.4167;0.3212;0.2142;0.1533;0.1712;0.2581;
%     0.3675 ;0.4407;0.4384;0.3618;0.2521;0.1680;0.1548;0.2195;0.3274;0.4206;0.4491]];
% hold on
% plot3(x_socle,y_socle,z_socle,'--r')
% hold off
% 
% robot_conf=homeConfiguration(robot);
% tform = getTransform(robot,robot_conf,'Body1','Body4')
% % 
% % ik = robotics.InverseKinematics('RigidBodyTree',robot);  
% % weights = [0 0 0 1 1 0];
% % initialguess = robot.homeConfiguration;
% % [configSoln,solnInfo] = ik('Body1',tform,weights,initialguess);
% % show(robot,configSoln)