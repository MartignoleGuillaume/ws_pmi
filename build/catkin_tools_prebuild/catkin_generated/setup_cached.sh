#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH="/home/gui/ws_pmi/devel/.private/catkin_tools_prebuild:$CMAKE_PREFIX_PATH"
export PWD="/home/gui/ws_pmi/build/catkin_tools_prebuild"
export ROSLISP_PACKAGE_DIRECTORIES="/home/gui/ws_pmi/devel/.private/catkin_tools_prebuild/share/common-lisp"
export ROS_PACKAGE_PATH="/home/gui/ws_pmi/build/catkin_tools_prebuild:$ROS_PACKAGE_PATH"