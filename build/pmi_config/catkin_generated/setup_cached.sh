#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH="/home/gui/ws_pmi/devel/.private/pmi_config:$CMAKE_PREFIX_PATH"
export LD_LIBRARY_PATH="/home/gui/ws_pmi/devel/.private/pmi_config/lib:$LD_LIBRARY_PATH"
export PKG_CONFIG_PATH="/home/gui/ws_pmi/devel/.private/pmi_config/lib/pkgconfig:$PKG_CONFIG_PATH"
export PWD="/home/gui/ws_pmi/build/pmi_config"
export ROSLISP_PACKAGE_DIRECTORIES="/home/gui/ws_pmi/devel/.private/pmi_config/share/common-lisp:$ROSLISP_PACKAGE_DIRECTORIES"
export ROS_PACKAGE_PATH="/home/gui/ws_pmi/src/pmi_config:$ROS_PACKAGE_PATH"