#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH="/home/gui/ws_pmi/devel/.private/urdf_pmi_final:$CMAKE_PREFIX_PATH"
export PWD="/home/gui/ws_pmi/build/urdf_pmi_final"
export ROSLISP_PACKAGE_DIRECTORIES="/home/gui/ws_pmi/devel/.private/urdf_pmi_final/share/common-lisp:$ROSLISP_PACKAGE_DIRECTORIES"
export ROS_PACKAGE_PATH="/home/gui/ws_pmi/src/urdf_pmi_final:$ROS_PACKAGE_PATH"